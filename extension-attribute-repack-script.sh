#!/bin/sh
#
# $ extension-attribute-repack-script.sh PACKAGE_NAME PACKAGE_VERSION
#
# There is the brief explanation how this script works.
#
# unpack zip archive
# find .NET 2.0 DLLs
# if none found: print error and exit
# else:
# foreach found DLL:
# | disassemble it
# | if there is ExtensionAttribute class:
# | | remove it
# | | add reference to ExtensionAttributeAssembly
# | | assemble it back
# | | replace original file with assembled one
# |
#
# alter modifiac
#
# This script requires the following commands to be in PATH:
#  'unzip''monodis' 'ilasm' 'diff' 'sed' in addition to 'nuget.exe'
#
#
set -o errexit
set -o nounset
set -o pipefail

# Rebuilds assembly file in-place, effectively removing ExtensionAttribute
# class from it and adding an external reference to another assembly. If this
# assembly file neither declares its own ExtensionAttribute class, nor uses it,
# then the file left unchanged and function return -1.
relinkAssembly() {
    LibraryFile="$1"
    RefAssemblyName="$2"
    RefAssemblyVersion="$3"
    RefAssemblyPubKey="$4"

    # These variables are here in order not to mess the code below.
    AssemblyDecl=\
        ".assembly extern $RefAssemblyName\n{\n"
        ".ver $RefAssemblyVersion\n"
        ".publickeytoken = $RefAssemblyPubKey\n }\n"
    CutMarker="^\s*.class \w+ \w+ \w+ \w+ \w+ ExtensionAttribute"
    EndMarker="// end of class System.Runtime.CompilerServices.ExtensionAttribute"
    SearchKey="System.Runtime.CompilerServices.ExtensionAttribute"
    ReplacVal="[$RefAssemblyName]System.Runtime.CompilerServices.ExtensionAttribute"

    # Create a temporary file for disassembled code. We must create it anyway
    # because 'ilasm' cannot work with pipes.
    TmpOriginalListing=`mktemp`
    trap "rm '$TmpOriginalListing'" 0

    TmpListing=`mktemp`
    trap "rm '$TmpListing'" 0

    # Disassemble source DLL to IL assembler code.
    monodis "$LibraryFile" > "$TmpOriginalListing"

    # Preprocess assembler code.
    sed -e "/$CatMarker/,/$EndMarker/d" \
        -e "s/$SearchKey/$ReplacVal/g" < "$TmpOriginalListing" >"$TmpListing"

    diff -u "$TmpOriginalListing" "$TmpListing" || \
        return -1

    # Prepend code to reference assembly containing ExtensionAttribute.
    echo "$AssemblyReferenceDecl" > "$TmpOriginalListing"
    cat "$TmpListing" >> "$TmpOriginalListing"

    TmpRebuiltLibrary=`mktemp`
    trap "rm '$TmpRebuiltLibrary'" 0

    ilasm /dll /output:"$TmpRebuiltLibrary" "$TmpOriginalListing"
    cp "$TmpRebuiltLibrary" "$LibraryFile"
}


relinkPackageFile() {
    PackageFile="$1"
    RefAssemblyName="$2"
    RefAssemblyVersion="$3"
    RefAssemblyPubKey="$4"
    PackageNameSuffix="$5"
    PackageDescription="$6"

    TmpPkgDir=`mktemp -d`
    trap "rm -rf '$TmpPkgDir'" 0

    unzip "$PackageFile" -x "$PkgTempDir"

    [ -d "$TmpPkgDir/lib/net20" ] || \
         return -1

    Changed=0
    for LibFile in "$TmpPkgDir/lib/net20"/*.[dD][lL][lL] ; do
        relinkAssemblyFile "$LibFile" \
            "$RefAssemblyName" \
            "$RefAssemblyVersion" \
            "$RefAssemblyPubKey" && Changed=1 || { [ "$?" == '-1' ] || false }
    done

    [ Changed == 1 ] || \
        return -1

    # alter package descrptions
    # alter package dependencies

}
